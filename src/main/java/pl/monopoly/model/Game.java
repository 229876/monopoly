package pl.monopoly.model;

import pl.monopoly.model.fields.*;

import java.util.List;
import java.util.Random;

public class Game {

    private final Board board;
    private final List<Player> players;
    private Player activePlayer;
    private int activePlayerDubletCount = 0;

    public Game(List<Player> players, Board board) {
        this.players = players;
        activePlayer = players.get(0);
        this.board = board;
    }

    public void removePlayer(Player player) {
        players.remove(player);
    }

    public boolean rollDice(int dice1, int dice2, boolean isDoublet) {

        int dotsSum = dice1 + dice2;

        if (isDoublet) {
            activePlayerDubletCount++;

            if (activePlayerDubletCount == 3) {
                activePlayerDubletCount = 0;
                activePlayer.setInJail(true);
                activePlayer.setTimeInJail(1);
                activePlayer.setPosition(10);
                return false;
            }
        }

        if (!activePlayer.isInJail()) {
            if (activePlayer.getPosition() + (dotsSum) > board.getBoardSize()) {
                int pos = activePlayer.getPosition() + (dotsSum) - board.getBoardSize();
                activePlayer.setPosition(pos);
                activePlayer.setTimeInJail(1);
            } else {
                activePlayer.move(dotsSum);
            }

            return true;

        } else {
            if (JailField.throwDouble(activePlayer, dice1, dice2)) {
                activePlayer.move(dotsSum);
                return true;
            }
            return false;
        }
    }

    public void useJailCard() {
        JailField.useJailCard(activePlayer);
    }

    public Board getBoard() {
        return board;
    }

    public boolean buyField() {
        PropertyField field = (PropertyField) (board.getFieldByID(activePlayer.getPosition()));
        return activePlayer.buyProperty(field);
    }

    public List<Player> getPlayers() {
        return players;
    }

    // Sprawdzanie czy ktoś wygrał
    public boolean IsGameOver() {
        return players.size() == 1;
    }

    // Metoda akcji po każdym ruchu gracza
    public String action() {
        Field field = board.getFields().get(activePlayer.getPosition());
        if (field.getClass() == Chance.class) {
            return ((Chance)field).trigger(activePlayer, players, board);

        } else if (field.getClass() == CommunityChest.class) {
            return ((CommunityChest)field).trigger(activePlayer, players, board);

        } else {
            field.trigger(activePlayer);
            return "";
        }
    }

    // Przełączanie gracza
    public void nextPlayer() {
        activePlayer = players.get((players.indexOf(activePlayer) + 1) % players.size());
    }

    public Player getActivePlayer() {
        return activePlayer;
    }
}
