package pl.monopoly.model;

import pl.monopoly.model.fields.PropertyField;
import pl.monopoly.model.fields.ColorPropertyField;

import java.util.LinkedList;
import java.util.List;

public class Player {

    private final String name;
    private final String color;
    private float cash;
    private int position;
    private boolean isBankrupt;
    private boolean isInJail;
    private int timeInJail;
    private boolean jailcard;

    private final List<PropertyField> properties;

    public Player(String name, String color, float startingCash) {
        this.name = name;
        this.color = color;
        this.cash = startingCash;
        position = 0;
        isBankrupt = false;
        isInJail = false;
        jailcard = false;
        timeInJail = 0;
        properties = new LinkedList<>();
    }

    // Konstruktor do tworzenia obiektu gracza w przypadku wczytywania zapisanej gry
    public Player(String name, String color, float cash, int position, boolean isBankrupt, boolean isInJail, int timeInJail,
                  List<PropertyField> properties) {
        this.name = name;
        this.color = color;
        this.cash = cash;
        this.position = position;
        this.isBankrupt = isBankrupt;
        this.isInJail = isInJail;
        this.timeInJail = timeInJail;
        this.properties = properties;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

    public int getTimeInJail() {
        return timeInJail;
    }

    public void setTimeInJail(int timeInJail) {
        this.timeInJail = timeInJail;
    }

    public boolean isInJail() {
        return isInJail;
    }

    public boolean getJailcard() {
        return jailcard;
    }

    public void setJailcard(boolean jailcard) {
        this.jailcard = jailcard;
    }

    @Override
    public String toString() {
        String s = "Nazwa Gracza: " + name;
        s = s + "\n" + "Stan Konta: " + cash + "\nAktualna Pozycja: " + position;

        return s;
    }

    public void setInJail(boolean inJail) {
        isInJail = inJail;
    }

    public void move(int steps) {
        position += steps;
    }

    public void addCash(float cash) {
        this.cash += cash;
    }

    public void takeCash(float cash) {
        if (cash > this.cash) {
            isBankrupt = true;
        } else {
            this.cash -= cash;
        }
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    public float getCash() {
        return cash;
    }

    void removeProperty(ColorPropertyField property) {
        properties.remove(property);
        property.setOwner(null);
    }

    public boolean buyProperty(PropertyField propertyField) {
        if ((propertyField.getCost() > cash) || propertyField.getOwner() != null) {
            return false;
        } else {
            properties.add(propertyField);
            propertyField.setOwner(this);
            takeCash(propertyField.getCost());
            return true;
        }
    }

    public void pledgeProperty(ColorPropertyField property) {
        property.setPledged(true);
        addCash(property.getMortgageValue());
    }

    public boolean unPledgeProperty(ColorPropertyField property) {
        if (property.getMortgageValue() * 1.1 > cash) {
            return false;
        } else {
            takeCash(property.getMortgageValue() * 1.1f);
            property.setPledged(false);
            return true;
        }
    }

    public boolean getBankrupt() {
        return this.isBankrupt;
    }

    public List<PropertyField> getProperties() {
        return properties;
    }
}