/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.monopoly.model.dao;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import pl.monopoly.model.Player;



public class FilePlayerDao implements Dao<Player> {
    private String nazwa;

    public FilePlayerDao(String name) {
        this.nazwa = name;
    }

    @Override
    public Player read() {
        Player obj = null;
        try (FileInputStream fileIn = new FileInputStream(nazwa);
             ObjectInputStream objectIn = new ObjectInputStream(fileIn)) {
            obj = (Player) objectIn.readObject();
        } catch (ClassNotFoundException | IOException ex) {
            throw new RuntimeException(ex);
        }
    return obj;
    }


    @Override
    public void write(Player o) {
        try (FileOutputStream fileOut = new FileOutputStream(nazwa);
             ObjectOutputStream objectOut = new ObjectOutputStream(fileOut)) {
            objectOut.writeObject(o);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void close() throws Exception {
        throw new RuntimeException("Not supported yet.");
    }
    
}
