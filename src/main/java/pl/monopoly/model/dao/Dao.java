/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.monopoly.model.dao;

import pl.monopoly.model.exception.DaoException;

import java.util.List;


public interface Dao<T> extends AutoCloseable {
    public T read() throws DaoException;
    
    void write(T obj) throws DaoException;
}
