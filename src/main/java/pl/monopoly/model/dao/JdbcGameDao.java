/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.monopoly.model.dao;

import pl.monopoly.model.Board;
import pl.monopoly.model.Player;
import pl.monopoly.model.exception.DatabaseException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import pl.monopoly.model.Game;
import pl.monopoly.model.fields.Chance;
import pl.monopoly.model.fields.ColorPropertyField;
import pl.monopoly.model.fields.CommunityChest;
import pl.monopoly.model.fields.Field;
import pl.monopoly.model.fields.FieldType;
import pl.monopoly.model.fields.PropertyField;
import pl.monopoly.model.fields.PropertyType;
import pl.monopoly.model.fields.TransportationField;
import pl.monopoly.model.fields.UtilityPropertyField;


public class JdbcGameDao implements Dao<Game> {

    private static String URL = "jdbc:postgresql://localhost:5432/monopoly";
    private static String USER = "postgres";
    private static String PASSWORD = "postgres";
    private String file;
    List<Player> players;
    Board board;

    private Connection connection;
    private Statement statement;
    private ResultSet result;

    private int board_id=0;
    private int first_player_id=0;
    private int number_of_players=4;


    public void setBoardId(int board_id) {
        this.board_id = board_id;
    }
    
    public void setFirstPlayerId(int first_player_id) {
        this.first_player_id = first_player_id;
    }
    
    public void setNumberOfPlayers(int number_of_players) {
        this.number_of_players = number_of_players;
    }
    
    
    
    

    public JdbcGameDao() throws DatabaseException {
        try {
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
            statement = connection.createStatement();
            connection.setAutoCommit(false);
        } catch(SQLException e) {
            throw new DatabaseException(e, "DatabaseError");
        }

    }

    public Connection makeConnection (String URL, String USER, String PASSWORD) throws DatabaseException {
        Connection connection;
        try {
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
        } catch (SQLException exception) {
            throw new DatabaseException(exception, "DatabaseError");
        }
        return connection;
    }


    @Override
    public Game read() throws DatabaseException {
        Game game = null;

        List<Player> players = new ArrayList<Player>();
        Player player = null;

        for(int i=first_player_id; i<=number_of_players;i++ ){
            try {
                ResultSet result = statement.executeQuery("SELECT * FROM public.\"gracz\" WHERE \"id_gracza\" ="+i);
                int ID_gracza;
                float cash;
                int position;
                boolean jailCard;
                boolean status;
                String name;
                String color;


                while (result.next()) {
                    ID_gracza = result.getInt("ID_gracza");
                    cash = result.getFloat("stan_konta");
                    position = result.getInt("pozycja");
                    jailCard = result.getBoolean("ekwipunek");
                    status = result.getBoolean("status");
                    name = result.getString("nazwa_gracza");
                    color = result.getString("kolor_gracza");

                    player =  new Player(name, color, cash, position, status, jailCard, 0,new ArrayList<>());
                }
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
            players.add(player);
        }


        Board board = null;
        int ID_planszy;
        int rozmiar_planszy =0;
        float starting_cash =0;
        List<Field> fields = new ArrayList<Field>();
        try {
            ResultSet result = statement.executeQuery("SELECT * FROM public.\"plansza\" WHERE \"id_planszy\" = "+board_id);

            while (result.next()) {
                ID_planszy = result.getInt("id_planszy");
                rozmiar_planszy = result.getInt("rozmiar_planszy");
                starting_cash = result.getFloat("starting_cash");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

        int id_pola=0;
        int wsk_do_pola=0;
        int id_wlasciciela=0;
        boolean czy_zastawiony=false;
        PropertyField propertyField = null;

        for(int i=0; i<=rozmiar_planszy;i++){
            try {
                ResultSet result = statement.executeQuery("SELECT * FROM public.\"pola_w_grze\" WHERE \"id_pola\" = "+i);

                while (result.next()) {
                    id_pola = result.getInt("id_pola");
                    wsk_do_pola = result.getInt("wsk_do_pola");
                    id_wlasciciela = result.getInt("id_wlasciciela");
                    czy_zastawiony = result.getBoolean("czy_zastawiony");
                }
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
            for(int j= first_player_id; j<=number_of_players; j++){
                for(int ii= 0; ii<=rozmiar_planszy; ii++){
                    if(j == id_wlasciciela){
                        if (i == 0) {}
                        else if (i == 10) {}
                        else if (i == 20) {}
                        else if (i == 30) {}
                        else if (i == 4 || i == 38) {}
                        else if (i == 5 || i == 15 || i == 25 || i == 35) {
                        players.get(j).addCash(50);
                        propertyField = new TransportationField("Dworzec", i, FieldType.PROPERTY,
                        50, new float[]{10, 20, 30, 40}, 25, PropertyType.TRANSPORTATION);
                        propertyField.setPledged(czy_zastawiony);
                        players.get(j).buyProperty(propertyField);
                        }
                        else if (i == 2 || i == 17 || i == 33) {}
                        else if (i == 7 || i == 22 || i == 36) {}
                        else {
                        players.get(j).addCash(50);
                        propertyField = new ColorPropertyField("P" + i, "blue", i, 50, 10, 20,
                        new float[]{10, 20, 30, 40, 50, 60}, 20, FieldType.PROPERTY, PropertyType.COLOR_PROPERTY);
                        propertyField.setPledged(czy_zastawiony);
                        players.get(j).buyProperty(propertyField);
                        }
                    }
                }
            }

        }

        board =  new Board(rozmiar_planszy,fields ,starting_cash);
        game = new Game(players,board);

        return game;



    }

    @Override
    public void write(Game game) throws DatabaseException {
        Connection connection = makeConnection(URL, USER, PASSWORD);
    }

    @Override
    public void close() throws DatabaseException {
        try {
            connection.close();
            statement.close();
            result.close();
        } catch(SQLException e) {
            throw new DatabaseException(e, "DatabaseError");
        }
    }

}