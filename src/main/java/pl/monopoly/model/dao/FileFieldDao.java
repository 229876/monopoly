/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.monopoly.model.dao;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import pl.monopoly.model.fields.Field;


public class FileFieldDao implements Dao<Field> {
    private String nazwa;

    public FileFieldDao(String name) {
        this.nazwa = name;
    }

    @Override
    public Field read() {
        Field obj = null;
        try (FileInputStream fileIn = new FileInputStream(nazwa);
             ObjectInputStream objectIn = new ObjectInputStream(fileIn)) {
            obj = (Field) objectIn.readObject();
        } catch (ClassNotFoundException | IOException ex) {
            throw new RuntimeException(ex);
        }
    return obj;
    }


    @Override
    public void write(Field o) {
        try (FileOutputStream fileOut = new FileOutputStream(nazwa);
             ObjectOutputStream objectOut = new ObjectOutputStream(fileOut)) {
            objectOut.writeObject(o);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void close() throws Exception {
        throw new RuntimeException("Not supported yet.");
    }
}
