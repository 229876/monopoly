/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.monopoly.model.exception;

/**
 *
 * @author mj200
 */
public class DatabaseException extends DaoException{
    
    public DatabaseException(Throwable cause, String databaseError) {
        super(cause);
    }
    
}