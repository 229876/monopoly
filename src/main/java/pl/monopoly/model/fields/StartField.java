package pl.monopoly.model.fields;

import pl.monopoly.model.Player;

public class StartField extends Field {

    private final int prize;

    public StartField(String name, int position, int prize, FieldType fieldType) {
        super(name, position,fieldType);
        this.prize = prize;
    }

    @Override
    public void trigger(Player player) {
        // Wejście na to pole nie wywyołuje żadnej akcji
    }
}
