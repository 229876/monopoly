package pl.monopoly.model.fields;

import java.util.Random;

public class UtilityPropertyField extends PropertyField {

    public UtilityPropertyField(String name, int position, FieldType fieldType, float cost,
                                float[] rentCost, float mortgageValue, PropertyType propertyType) {
        super(name, position, fieldType, cost, rentCost, mortgageValue, propertyType);
    }

    @Override
    public float calculateRentCost() {
        int utilityPropertyCount = 0;

        for (PropertyField propertyField : getOwner().getProperties()) {
            if (propertyField.getPropertyType().equals(PropertyType.UTILITY)) {
                utilityPropertyCount++;
            }
        }

        int dice1 = new Random().nextInt(6) + 1;
        int dice2 = new Random().nextInt(6) + 1;

        if (utilityPropertyCount == 1) {
            return 4 * (dice1 + dice2) * getRentCost()[0];
        } else {
            return 10 * (dice1 + dice2) * getRentCost()[0];
        }
    }
}
