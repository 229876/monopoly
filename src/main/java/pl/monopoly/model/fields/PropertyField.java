package pl.monopoly.model.fields;

import pl.monopoly.model.Player;

public abstract class PropertyField extends Field {

    private final float cost;
    private final float mortgageValue;
    private final float[] rentCost;
    private final PropertyType propertyType;
    private Player owner;
    private boolean isPledged;

    public PropertyField(String name, int position, FieldType fieldType, float cost,
                         float[] rentCost, float mortgageValue, PropertyType propertyType) {

        super(name, position, fieldType);
        this.cost = cost;
        this.mortgageValue = mortgageValue;
        this.rentCost = rentCost;
        this.propertyType = propertyType;
        owner = null;
        isPledged = false;
    }

    // Koszt zakupu posesji
    public float getCost() {
        return cost;
    }

    public float[] getRentCost() {
        return rentCost;
    }

    // Podaj koszt czynszu
    public abstract float calculateRentCost();

    // Wartość zastawienia posesji
    public float getMortgageValue() {
        return mortgageValue;
    }

    public PropertyType getPropertyType() {
        return propertyType;
    }

    // Podaj właściciela
    public Player getOwner() {
        return owner;
    }

    // Ustaw właściciela posesji
    public void setOwner(Player player) {
        owner = player;
    }

    public void setPledged(boolean isPledged) {
        this.isPledged = isPledged;
    }

    public boolean isPledged() {
        return isPledged;
    }

    // Płacenie czynszu
    public void payRent(Player player) {
        if (owner != null && !isPledged) {
            float rent = calculateRentCost();
            player.takeCash(rent);
            owner.addCash(rent);
        }
    }

    @Override
    public void trigger(Player player) {
        payRent(player);
    }
}
