package pl.monopoly.model.fields;

public enum FieldType {
    START, TAX, JAIL, PROPERTY, GOTOJAIL, PARKING, CHANCE
}