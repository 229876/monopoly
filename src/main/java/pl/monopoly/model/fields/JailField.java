package pl.monopoly.model.fields;

import pl.monopoly.model.Player;

import java.util.Random;

public class JailField extends Field {

    public JailField(String name, int position, FieldType fieldType) {
        super(name, position,fieldType);
    }

    @Override
    public void trigger(Player player) {
        // Wejście na to pole nie wywyołuje żadnej akcji
    }

    private static void getOutOfJail(Player player) {
        player.setInJail(false);
        player.setTimeInJail(0);
    }

    //uzycie karty "wyjście z więzienia"
    public static boolean useJailCard(Player player) {
        if (player.getJailcard()) {
            player.setJailcard(false);
            getOutOfJail(player);
            return true;

        } else {
            return false;
        }
    }

    //wyrzucenie dubli
    public static boolean throwDouble(Player player, int dice1, int dice2) {
        if (dice1 == dice2) {
            getOutOfJail(player);
            return true;

        } else {
            return false;
        }
    }

    //wyjscie za zaplata
    public static boolean payFine(Player player) {
        if (player.getCash() >= 50) {
            getOutOfJail(player);
            return true;

        } else {
            return false;
        }
    }
}
