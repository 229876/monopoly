package pl.monopoly.model.fields;

import pl.monopoly.model.Player;

public class GoToJailField extends Field {

    public GoToJailField(String name, int position, FieldType fieldType) {
        super(name, position, fieldType);
    }

    @Override
    public void trigger(Player player) {
        player.setInJail(true);
        player.setTimeInJail(1);
        player.setPosition(10);
    }
}
