package pl.monopoly.model.fields;

import pl.monopoly.model.Board;
import pl.monopoly.model.Player;

import java.util.List;
import java.util.Random;

public class CommunityChest extends Field {

    public CommunityChest(String name, int position, FieldType fieldType) {
        super(name, position,fieldType);
    }

    @Override
    public void trigger(Player player) {

    }

    public String trigger(Player player, List<Player> players, Board board) {
        return "co" + ChooseCommunityChest(player, players, board);
    }

    private String ChooseCommunityChest(Player player, List<Player> players, Board board){
        Random generator = new Random();

        switch (generator.nextInt(16)) {
            case 0:
                //1.Wymieniasz elektrykę w swoich nieruchomościach. Zapłać: M 40 za każda posesje
                int propertyField = 0;
                for(int i = 0; i < board.getBoardSize(); i++) {
                    if (board.getFieldByID(i) instanceof PropertyField) {
                        if (player == ((PropertyField) board.getFieldByID(i)).getOwner()) {
                            propertyField++;
                        }
                    }
                }
                player.takeCash(propertyField*40);
                return "Wymieniasz elektrykę w swoich nieruchomościach. Zapłać: M 40 za każda posesje";
            case 1:
                //2.Zająłeś 2 miejsce w konkursie piękności. Pobierz M 10
                player.addCash( 10);
                return "Zająłeś 2 miejsce w konkursie piękności. Pobierz M 10";
            case 2:
                //3.Błąd bankowy na twoim koncie! Pobierz M 200
                player.addCash( 200);
                return "Błąd bankowy na twoim koncie! Pobierz M 200";
            case 3:
                //4.Wyjdź bezpłatnie z więzienia. (Tę kartę możesz zatrzymać do późniejszego wykorzystania lub sprzedaży)
                player.setJailcard(true);
                return "Wyjdź bezpłatnie z więzienia. (Tę kartę możesz zatrzymać do późniejszego wykorzystania lub sprzedaży)";
            case 4:
                //5.Idź do więzienia. Nie przechodź przez START. Nie pobieraj M 200
                board.getFieldByID(10).trigger(player);
                return "Idź do więzienia. Nie przechodź przez START. Nie pobieraj M 200";
            case 5:
                //6.Masz urodziny! Pobierz M 10 od każdego gracza.
                for (int i = 0; i < players.size(); i++) {
                    players.get(i).takeCash(10);
                }
                player.addCash(10 * players.size());
                return "Masz urodziny! Pobierz M 10 od każdego gracza.";
            case 6:
                //7.Fundusz Zdrowotny. Pobierz M 100
                player.addCash(100);
                return "Fundusz Zdrowotny. Pobierz M 100";
            case 7:
                //8.Zapłać za wizytę u dentysty- M 100
                player.takeCash(100);
                return "Zapłać za wizytę u dentysty- M 100";
            case 8:
                //9.Zapłać czesne-M 50
                player.takeCash(50);
                return "Zapłać czesne-M 50";
            case 9:
                //10.Przejdź na START ( PObierz M 200)
                player.addCash( 200);
                player.setPosition(0);
                return "Przejdź na START ( PObierz M 200)";
            case 10:
                //11.Wyprzedaż! Pobierz M 50
                player.addCash( 50);
                return "Wyprzedaż! Pobierz M 50";
            case 11:
                //12.Zapłać za wizytę Lekarską-M 50
                player.takeCash(50 );
                return "Zapłać za wizytę Lekarską-M 50";
            case 12:
                //13.Zwrot podatku. Pobierz M 20
                player.addCash(20 );
                return "Zwrot podatku. Pobierz M 20";
            case 13:
                //14.Otrzymujesz M 25 za porady finansowe
                player.addCash( 25 );
                return "Otrzymujesz M 25 za porady finansowe";
            case 14:
                player.addCash(100 );
                //15.Odziedziczyłeś spadek. Pobierz M 100
                return "Odziedziczyłeś spadek. Pobierz M 100";
            case 15:
                //16.Dostałeś premię! Pobierz M 100
                player.addCash( 100 );
                return "Dostałeś premię! Pobierz M 100";
        }
        return null;
    }
}