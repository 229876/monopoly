package pl.monopoly.model.fields;

public enum PropertyType {
    COLOR_PROPERTY, TRANSPORTATION, UTILITY
}