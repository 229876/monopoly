package pl.monopoly.model.fields;

import pl.monopoly.model.Player;

public abstract class Field {

    private final String name;
    private final int position;
    private final FieldType fieldType;

    public Field(String name, int position, FieldType fieldType) {
        this.name = name;
        this.position = position;
        this.fieldType = fieldType;
    }

    public String getName() {
        return name;
    }

    public FieldType getFieldType() {
        return fieldType;
    }

   public int getPosition() {
        return position;
   }

    // akcja po wejściu gracza na pole
    public abstract void trigger(Player player);
}