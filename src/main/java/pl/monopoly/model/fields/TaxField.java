package pl.monopoly.model.fields;

import pl.monopoly.model.Player;

public class TaxField extends Field {

    private final float fee;

    public TaxField(String name, int position, float fee, FieldType fieldType) {
        super(name, position, fieldType);
        this.fee = fee;
    }

    // Pobieranie podatku
    public void PayFee(Player player) {
        player.takeCash(this.fee);
    }

    public float getFee() {
        return fee;
    }

    @Override
    public void trigger(Player player) {
        PayFee(player);
    }
}
