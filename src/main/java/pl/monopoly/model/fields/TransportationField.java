package pl.monopoly.model.fields;

public class TransportationField extends PropertyField {

    public TransportationField(String name, int position, FieldType fieldType,
                               float cost, float[] rentCost, float mortgageValue, PropertyType propertyType) {
        super(name, position, fieldType, cost, rentCost, mortgageValue, propertyType);
    }

    // Wyliczanie kosztu czynszu na podstawie ilości posiadanych nieruchomości typu transport
    @Override
    public float calculateRentCost() {
        int transportationFieldCount = 0;
        for (PropertyField propertyField : getOwner().getProperties()) {
            if (propertyField.getFieldType().equals(PropertyType.TRANSPORTATION)) {
                transportationFieldCount++;
            }
        }
        return getRentCost()[transportationFieldCount];
    }
}
