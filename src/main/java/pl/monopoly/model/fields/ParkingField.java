package pl.monopoly.model.fields;

import pl.monopoly.model.Player;

public class ParkingField extends Field {

    public ParkingField(String name, int position, FieldType fieldType) {
        super(name, position, fieldType);
    }

    @Override
    public void trigger(Player player) {
        // Wejście na to pole nie wywyołuje żadnej akcji
    }
}
