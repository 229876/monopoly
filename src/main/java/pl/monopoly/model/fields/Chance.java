package pl.monopoly.model.fields;

import pl.monopoly.model.Board;
import pl.monopoly.model.Player;

import java.util.List;
import java.util.Random;

public class Chance extends Field {

    public Chance(String name, int position, FieldType fieldType) {
        super(name, position, fieldType);
    }

    @Override
    public void trigger(Player player) {

    }

    public String trigger(Player player, List<Player> players, Board board) {
        return "ch" + ChooseChance(player, players, board);
    }

    int current_card = 16;
    int[] OrderInTheCards = new int [16];

    public String ChooseChance(Player player, List<Player> players, Board board) {
        //jesli jest to pierwszy wybor karty ustala kolejke
        if (current_card == 16) {
            for (int i = 0; i < 16; i++) {
                OrderInTheCards[i] = i;
            }
            for (int i = 0; i < 16; i++) {
                int j = new Random().nextInt(15);
                int a = OrderInTheCards[i];
                this.OrderInTheCards[i] = OrderInTheCards[j];
                this.OrderInTheCards[j] = a;
                this.current_card = 1;
            }
        }
        //jesli to nie jest pierwszy wybor karty bierze nastepna
        else {
            current_card = (current_card + 1) % 16;
        }

        switch (OrderInTheCards[current_card]) {
            case 0:
                //1.Idż na ulice płowiecką jeśli przejdziesz przez START pobierz M 200
                player.setPosition(11);
                if (player.getPosition() > 11) {
                    player.addCash(200);
                }
                return "Idż na ulice płowiecką jeśli przejdziesz przez START pobierz M 200";
            case 1:
                //2.Idź na Dworzec Zachodni. Jeśli przejdziesz przez START pobierz M 200
                player.setPosition(15);
                if (player.getPosition() > 15) {
                    player.addCash(200);
                }
                return "Idź na Dworzec Zachodni. Jeśli przejdziesz przez START pobierz M 200";
            case 2:
                //3.Zrobiłeś błąd w kalkulacjach podatkowych. Zapłać każdemu graczowi M 50
                for (int i = 0; i < players.size(); i++) {
                    players.get(i).addCash(50);
                }
                player.takeCash(50 * players.size());
                return "Zrobiłeś błąd w kalkulacjach podatkowych. Zapłać każdemu graczowi M 50";
            case 3:
            case 4:
                //3,4.Idź na najbliższy Dworzec kolejowy. Jeśli nie jest zajęty możesz go kupić od banku. Jeśli jest już zajęty zapłać właścicielowi odpowiednią sumę.

                if (player.getPosition() < 5 || player.getPosition() > 35) {
                    player.setPosition(5);
                    board.getFieldByID(5).trigger(player);
                }
                if (player.getPosition() < 15) {
                    player.setPosition(15);
                    board.getFieldByID(15).trigger(player);
                }
                if (player.getPosition() < 25) {
                    player.setPosition(25);
                    board.getFieldByID(25).trigger(player);
                }
                if (player.getPosition() < 35) {
                    player.setPosition(35);
                    board.getFieldByID(35).trigger(player);
                }
                return "Idź na najbliższy Dworzec kolejowy. Jeśli nie jest zajęty możesz go kupić od banku. Jeśli jest już zajęty zapłać właścicielowi odpowiednią sumę.";
            case 5:
                //6.Idź do Elektrowni.Jeśli jest już zajęta zapłać właścicielowi odpowiednią sumę
                player.setPosition(12);
                player.takeCash(((PropertyField) board.getFieldByID(12)).calculateRentCost());
                return "Idź do Elektrowni.Jeśli jest już zajęta zapłać właścicielowi odpowiednią sumę";
            case 6:
                // 7.Przeprowadź remont generalny wszystkich swoich nieruchomości: Zapłać za każda posesje M 25
                int propertyField = 0;
                for (int i = 0; i < board.getBoardSize(); i++) {
                    if (board.getFieldByID(i) instanceof PropertyField) {
                        if (player == ((PropertyField) board.getFieldByID(i)).getOwner()) {
                            propertyField++;
                        }
                    }
                }
                player.takeCash(propertyField * 25);
                return "Przeprowadź remont generalny wszystkich swoich nieruchomości: Zapłać za każda posesje M 25";
            case 7:
                //8.Idź do więzienia. Nie przechodź przez START. Nie pobieraj M 200
                board.getFieldByID(10).trigger(player);
                return "Idź do więzienia. Nie przechodź przez START. Nie pobieraj M 200";
            case 8:
                //9.Wyjdź bezpłatnie z więzienia. (Tę kartę możesz zatrzymać do późniejszego wykorzystania lub sprzedaży)
                player.setJailcard(true);
                return "Wyjdź bezpłatnie z więzienia. (Tę kartę możesz zatrzymać do późniejszego wykorzystania lub sprzedaży)";
            case 9:
                //10.Idź na Plac Wilson. Jeśli przejdziesz przez START pobierz M 200
                player.setPosition(24);
                if (player.getPosition() > 24) {
                    player.addCash(200);
                }
                return "Idź na Plac Wilson. Jeśli przejdziesz przez START pobierz M 200";
            case 10:
                //11.Bank wypłaca Ci dywidwndę. Pobierz M 50
                player.addCash(50);
                return "Bank wypłaca Ci dywidwndę. Pobierz M 50";
            case 11:
                //12.Zwrot podatku.Pobierz M 150
                player.addCash(150);
                return "Zwrot podatku.Pobierz M 150";
            case 12:
                //13.Przejdź na START. (Pobierz M 200)
                player.setPosition(0);
                player.addCash(200);
                return "Przejdź na START. (Pobierz M 200)";
            case 13:
                //14.Zapłać grzywne - M 15
                player.takeCash(15);
                return "Zapłać grzywne - M 15";
            case 14:
                //15.Cofnij się o trzy pola.
                player.setPosition(player.getPosition() - 3);
                return "Cofnij się o trzy pola";
            case 15:
                // 16. Idź na Aleje Ujazdowskie
                player.setPosition(39);
                return "Idź na Aleje Ujazdowskie";
        }
        return null;
    }
}
