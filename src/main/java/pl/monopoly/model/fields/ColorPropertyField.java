package pl.monopoly.model.fields;

public class ColorPropertyField extends PropertyField {

    private final String color;
    private final float housesCost; // koszt domku
    private final float hotelCost;  // koszt hotelu
    private int propertyLevel;

    public ColorPropertyField(String name, String color, int position, float cost, float housesCost, float hotelCost,
                              float[] rentCost, float mortgageValue, FieldType fieldType, PropertyType propertyType) {
        super(name, position,fieldType, cost, rentCost, mortgageValue, propertyType);
        this.color = color;
        this.housesCost = housesCost;
        this.hotelCost = hotelCost;
        this.propertyLevel = 0;
    }

    public String getColor() {
        return color;
    }

    // Podaj koszt czynszu
    @Override
    public float calculateRentCost() {
        int colorPropertyCount = 0;

        if (getPropertyLevel() == 0) {
            for (PropertyField property : getOwner().getProperties()) {
                if (property.getPropertyType().equals(PropertyType.COLOR_PROPERTY)
                        && ((ColorPropertyField) property).getColor().equals(this.getColor())) {
                    colorPropertyCount++;
                }
            }
            if (colorPropertyCount == 3) {
                return getRentCost()[0] * 2;

            } else {
                return getRentCost()[0];
            }

        } else {
            return getRentCost()[propertyLevel];
        }
    }

    // Podaj poziom posesji ("ilość domków")
    public int getPropertyLevel() {
        return propertyLevel;
    }

    public float getHousesCost() {
        return housesCost;
    }

    public float getHotelCost() {
        return hotelCost;
    }

    // Ulepsz posesję ("kup domek")
    public void upgradeProperty() {
        if (propertyLevel < 5) {
            propertyLevel++;
        }
    }
}