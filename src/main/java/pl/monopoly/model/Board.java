package pl.monopoly.model;

import pl.monopoly.model.fields.Field;

import java.util.List;

public class Board {

    private final int boardSize;
    private final List<Field> fields;
    private final float startingCash;

    public Board(int boardSize, List<Field> fields, float startingCash) {
        this.boardSize = boardSize;
        this.fields = fields;
        this.startingCash = startingCash;
    }

    public int getBoardSize() {
        return boardSize;
    }

    public List<Field> getFields() {
        return fields;
    }

    public float getStartingCash() {
        return startingCash;
    }

    public Field getFieldByID(int ID){
        return fields.get(ID);
    }
}
