package pl.monopoly.view.controller;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import pl.monopoly.view.JavaFX;

import java.net.URL;
import java.util.ResourceBundle;

public class menuController implements Initializable {

    public AnchorPane rootPane;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
    }

    public void newGameBtnAction(ActionEvent actionEvent) {
        JavaFX.setScene("/fxml/newGame.fxml", "New Game");
    }

    public void loadGameBtnAction(ActionEvent actionEvent) {
        JavaFX.setScene("/fxml/loadGame.fxml", "Load Game");
    }

    public void exitGameBtnAction(ActionEvent actionEvent) {
        System.exit(0);
    }
}
