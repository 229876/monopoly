package pl.monopoly.view.controller;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import pl.monopoly.model.Game;
import pl.monopoly.model.Player;
import pl.monopoly.view.JavaFX;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

public class newGameController implements Initializable {

    public AnchorPane rootPane;
    public Button startGameBtn;
    public ComboBox<Integer> numberOfPlayersCB;
    public Pane playerNamesPane;
    public TextField firstPlayerNameTF;
    public TextField secondPlayerNameTF;
    public TextField thirdPlayerNameTF;
    public TextField fourthPlayerNameTF;
    public TextField fifthPlayerNameTF;
    public TextField sixthPlayerNameTF;
    public ComboBox<String> chooseBoardCB;

    private int numberOfPlayers;
    private static List<Player> players = new ArrayList<>();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        numberOfPlayersCB.getItems().addAll(Arrays.asList(2, 3, 4, 5, 6));
        chooseBoardCB.getItems().addAll(Arrays.asList("Polska"));
    }

    public void startGameAction(ActionEvent actionEvent) {

        String[] colors = new String[] {"red", "green", "yellow", "blue", "purple", "pink"};
        int i = 0;
        for (Node node : playerNamesPane.getChildren()) {
            if (i < numberOfPlayers) {
                players.add(new Player(((TextField)node).getText(), colors[i], 1500));
            }
            i++;
        }

        JavaFX.setScene("/fxml/inGame.fxml", "Monopoly");
    }

    public static List<Player> getPlayers() {
        return players;
    }

    public void numberOfPlayersCBOnAction(ActionEvent actionEvent) {
        numberOfPlayers = numberOfPlayersCB.getValue();

        int i = 0;
        for (Node node : playerNamesPane.getChildren()) {
            if (i < numberOfPlayers) {
                node.setDisable(false);

            } else {
                node.setDisable(true);
            }
            i++;
        }
    }

    public void chooseBoardCBOnAction(ActionEvent actionEvent) {
        // TODO: Wczytanie dostępnych rodzajów plansz z bazy danych
    }
}
