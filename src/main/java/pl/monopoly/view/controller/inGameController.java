package pl.monopoly.view.controller;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Bounds;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Sphere;
import javafx.scene.text.TextAlignment;
import pl.monopoly.model.Board;
import pl.monopoly.model.Game;
import pl.monopoly.model.Player;
import pl.monopoly.model.fields.*;

import java.net.URL;
import java.util.*;

public class inGameController implements Initializable {

    public Button pledgePropertyBtn;
    public Button unpledgePropertyBtn;
    public Button upgradePropertyBtn;
    public Button saveGameBtn;
    public Button useJailFreeCardBtn;
    public Label playerNameLabel;
    public Label cashLabel;
    public Label fieldNameLabel;
    public Label isInJailLabel;
    public Label playerPositionLabel;
    public Button payToLeaveJailBtn;
    public GridPane playerNamesLabel;
    @FXML
    private AnchorPane rootPane;

    @FXML
    private Button roll;

    @FXML
    private Button buy;

    ImageView leftDiceIV;
    ImageView rightDiceIV;

    private Game game;

    private int clickedFieldID = 0;
    private int clickedFieldID_before = 0;

    private int ii = 0;
    private boolean endOfTurn = false;
    private float theSize = 0.5f;

    private List<GridPane> guiFields = new ArrayList<>();
    private List<Circle> guiPlayers = new ArrayList<>();
    private Pane guiBoard;
    private Map<Integer, List<ImageView>> guiHouses = new HashMap<>();
    private Map<Integer, Circle> guiOwners = new HashMap<>();
    private GridPane guiClickedFieldCard;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        Board board = initBoard();
        List<Player> players;

        players = newGameController.getPlayers();

        game = new Game(players, board);

//        game.getActivePlayer().addCash(10000);
//        game.getActivePlayer().buyProperty((PropertyField) game.getBoard().getFieldByID(31));
//        game.getActivePlayer().buyProperty((PropertyField) game.getBoard().getFieldByID(32));
//        game.getActivePlayer().buyProperty((PropertyField) game.getBoard().getFieldByID(34));

        for (int i = 0; i < game.getPlayers().size(); i++) {
            int row = i / 2;
            int column = (i % 2) * 3;

            if (i < game.getPlayers().size()) {
                playerNamesLabel.add(new Label(game.getPlayers().get(i).getName()), column, row);

                Circle circle = new Circle(30 * theSize);
                circle.setFill(Color.web(game.getPlayers().get(i).getColor()));
                GridPane.setFillWidth(circle, true);
                GridPane.setFillHeight(circle, true);
                playerNamesLabel.add(circle, column + 1, row);
            }
        }


        printPlayerInfo();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();

        createBoard();

        for (Player player : game.getPlayers()) {

            Circle circle = new Circle();
            circle.setRadius(30 * theSize);
            circle.setFill(Color.web(player.getColor()));
            guiPlayers.add(circle);

            guiBoard.getChildren().add(circle);

            setGuiPlayerPosition(player);
        }
        buy.setDisable(true);
    }

    private void setGuiPlayerPosition() {
        setGuiPlayerPosition(game.getActivePlayer());
    }

    private void setGuiPlayerPosition(Player player) {

        for (Circle circle : guiPlayers) {
            if (circle.getFill().equals(Color.web(player.getColor()))) {

                Bounds bounds = guiFields.get(player.getPosition()).
                        localToScene(guiFields.get(player.getPosition()).getBoundsInLocal());

                double x = bounds.getMinX();
                double y = bounds.getMinY();

                circle.setTranslateY(y + 100 * theSize);
                circle.setTranslateX(x + 50 * theSize + circle.getRadius() + 20 * theSize * game.getPlayers().indexOf(player));
            }
        }
    }

    @FXML
    void buy(ActionEvent event) {
        Field field = game.getBoard().getFields().get(game.getActivePlayer().getPosition());
        if (field.getFieldType().equals(FieldType.PROPERTY)) {

            game.buyField();
            System.out.println(((PropertyField)game.getBoard().getFieldByID(game.getActivePlayer().getPosition())).getOwner().getName());
            printPlayerInfo();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();
            buy.setDisable(true);

            Bounds bounds = guiFields.get(game.getActivePlayer().getPosition()).
                    localToScene(guiFields.get(game.getActivePlayer().getPosition()).getBoundsInLocal());

            double x = bounds.getMinX();
            double y = bounds.getMinY();

            Circle circle = new Circle(15 * theSize);
            circle.setFill(Color.web(game.getActivePlayer().getColor()));

            guiBoard.getChildren().add(circle);
            guiOwners.put(field.getPosition(), circle);

            // Pola pionowe lewe
            if (field.getPosition() > 10 && field.getPosition() < 20) {
                circle.setTranslateX(x + 250 * theSize + 20 * theSize);
                circle.setTranslateY(y + 150 * theSize / 2);

            // Pola pionowe prawe
            } else if (field.getPosition() > 30 && field.getPosition() < 40) {
                circle.setTranslateX(x - 20 * theSize);
                circle.setTranslateY(y + 150 * theSize / 2);

            // Pola o góry
            } else if (field.getPosition() > 20 && field.getPosition() < 30) {
                System.out.println(x);
                System.out.println(y);
                System.out.println(x + 150 * theSize / 2);
                circle.setTranslateX(x + 150 * theSize / 2);
                circle.setTranslateY(y + 250 * theSize + 20 * theSize);

            // Pola u dołu
            } else {
                circle.setTranslateX(x + 150 * theSize / 2);
                circle.setTranslateY(y - 20 * theSize);
            }
        }
    }

    @FXML
    void nextTurn(ActionEvent event) {
        game.nextPlayer();
        printPlayerInfo();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();
        clearOptions();
        roll.setDisable(false);
        buy.setDisable(true);
        endOfTurn = false;

        if (game.getActivePlayer().isInJail() && game.getActivePlayer().getJailcard()) {
            useJailFreeCardBtn.setDisable(false);
        }
        if (game.getActivePlayer().isInJail() && game.getActivePlayer().getCash() >= 50) {
            payToLeaveJailBtn.setDisable(false);
        }
    }

    @FXML
    void roll(ActionEvent event) {

        clearOptions();

        int dice1 = new Random().nextInt(6) + 1;
        int dice2 = new Random().nextInt(6) + 1;

        leftDiceIV.setImage(new Image("/img/dice/dice" + dice1 + ".png"));
        rightDiceIV.setImage(new Image("/img/dice/dice" + dice2 + ".png"));

        boolean notInJail = game.rollDice(dice1, dice2, dice1 == dice2);
        setGuiPlayerPosition();
        if (game.getActivePlayer().getPosition() == 30) {
            communityChanceWindow("wi", "Idziesz do więzienia");
        }
        String output = game.action();
        setGuiPlayerPosition();
        // Otwieranie okienka z informacją zwrotną z pola szansy lub skrzyni społecznej
        if (!output.equals("")) {
            communityChanceWindow(output.substring(0, 2), output.substring(2));
        }

        printPlayerInfo();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();

        if (!notInJail) {
            buy.setDisable(true);
            roll.setDisable(true);
            endOfTurnOptions();
            setGuiPlayerPosition();
            return;
        }

        buy.setDisable(true);
        Field currentField = game.getBoard().getFieldByID(game.getActivePlayer().getPosition());
        if (currentField.getFieldType() == FieldType.PROPERTY) {
            if (((PropertyField)currentField).getOwner() == null) {
                buy.setDisable(false);
            } else {
                buy.setDisable(true);
            }
        }

        // Dublet
        if (dice1 == dice2) {

        } else {
            endOfTurn = true;
            endOfTurnOptions();
            roll.setDisable(true);
        }

        setGuiPlayerPosition();
        printPlayerInfo();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();
    }

    void printPlayerInfo() {

        playerNameLabel.setText(game.getActivePlayer().getName());
        cashLabel.setText(game.getActivePlayer().getCash() + "");
        fieldNameLabel.setText(game.getBoard().getFields().get(game.getActivePlayer().getPosition()).getName());
        playerPositionLabel.setText(game.getActivePlayer().getPosition() + "");
        if (game.getActivePlayer().isInJail()) {
            isInJailLabel.setText("Przebywa w więzieniu!");

        } else {
            isInJailLabel.setText("");
        }
    }

    private void createBoard() {
        float size = theSize;
        List<Field> fields = game.getBoard().getFields();

        guiBoard = new Pane();
        guiBoard.setPrefSize(9 * 150 * size + 2 * 250 * size, 9 * 150 * size + 2 * 250 * size); // Wymiary planszy
        guiBoard.setStyle("-fx-background-color: #cde6d0");

        // Ustawianie pól na planszy
        // 0
        {
            GridPane field = createGridPane(fields.get(0), 0, size);
            field.setTranslateX(9 * 150 * size + (250 * size));
            field.setTranslateY(9 * 150 * size + (250 * size));
            guiBoard.getChildren().add(field);
            guiFields.add(field);
        }

        // 1 - 9
        for (int i = 8; i > -1; i--) {
            GridPane field = createGridPane(fields.get(9 - i), 0, size);
            field.setTranslateX(i * 150 * size + (250 * size));
            field.setTranslateY(9 * 150 * size + (250 * size));
            guiBoard.getChildren().add(field);
            guiFields.add(field);
        }

        // 10
        {
            GridPane field = createGridPane(fields.get(10), 0, size);
            field.setTranslateY(9 * 150 * size + (250 * size));
            guiBoard.getChildren().add(field);
            guiFields.add(field);
        }

        // 11 - 19
        for (int i = 8; i > -1; i--) {
            GridPane field = createGridPane(fields.get(19 - i), 90, size);
            field.setTranslateX(50 * size);
            field.setTranslateY(i * 150 * size + (250 * size) + (-50 * size));
            guiBoard.getChildren().add(field);
            guiFields.add(field);
        }

        // 20
        {
            GridPane field = createGridPane(fields.get(20), 0, size);
            guiBoard.getChildren().add(field);
            guiFields.add(field);
        }

        // 21 - 29
        for (int i = 0; i < 9; i++) {
            GridPane field = createGridPane(fields.get(21 + i), 180, size);
            field.setTranslateX(i * 150 * size + (250 * size));
            guiBoard.getChildren().add(field);
            guiFields.add(field);
        }

        // 30
        {
            GridPane field = createGridPane(fields.get(30), 0, size);
            field.setTranslateX(9 * 150 * size + (250 * size));
            guiBoard.getChildren().add(field);
            guiFields.add(field);
        }

        // 31 - 39
        for (int i = 0; i < 9; i++) {
            GridPane field = createGridPane(fields.get(31 + i), -90, size);
            field.setTranslateX(9 * 150 * size + (250 * size) + (50 * size));
            field.setTranslateY(i * 150 * size + (250 * size) + (-50 * size));
            guiBoard.getChildren().add(field);
            guiFields.add(field);
        }
        // ====

        // Monopoly logo
        Image image = new Image("/img/fields/monopoly_logo.png");
        ImageView monopolyLogoIV = new ImageView(image);
        monopolyLogoIV.setFitHeight(2 * 150 * size);
        monopolyLogoIV.setFitWidth(5 * 150 * size);
        monopolyLogoIV.setTranslateX(250 * size + 2 * 150 * size);
        monopolyLogoIV.setTranslateY(250 * size + 150 * size);
        guiBoard.getChildren().add(monopolyLogoIV);


        // Lewa kostka (puste ImageView)
        leftDiceIV = new ImageView();
        leftDiceIV.setFitHeight(150 * size);
        leftDiceIV.setFitWidth(150 * size);
        leftDiceIV.setTranslateX(250 * size + 3 * 150 * size);
        leftDiceIV.setTranslateY(250 * size + 7 * 150 * size);
        guiBoard.getChildren().add(leftDiceIV);

        // Prawa kostka (puste ImageView)
        rightDiceIV = new ImageView();
        rightDiceIV.setFitHeight(150 * size);
        rightDiceIV.setFitWidth(150 * size);
        rightDiceIV.setTranslateX(250 * size + 5 * 150 * size);
        rightDiceIV.setTranslateY(250 * size + 7 * 150 * size);
        guiBoard.getChildren().add(rightDiceIV);


        // Karta posesji
        guiClickedFieldCard = new GridPane();
        guiClickedFieldCard.setTranslateX(250 * size + 5 * 150 * size);
        guiClickedFieldCard.setTranslateY(250 * size + 7 * 150 * size);
        guiBoard.getChildren().add(guiClickedFieldCard);


        rootPane.getChildren().add(guiBoard);
    }

    private GridPane createGridPane(Field field, int rotation, float size) {

        GridPane gridPane = new GridPane();
        gridPane.setStyle("-fx-border-color: black; -fx-border-width: 1px");

        // Jeśli konstruujemy to węższe pole
        if (field.getFieldType().equals(FieldType.TAX) || field.getFieldType().equals(FieldType.PROPERTY)
                || field.getFieldType().equals(FieldType.CHANCE)) {

            gridPane.getColumnConstraints().add(new ColumnConstraints(150 * size));       // szerokość

            Label nameLabel = new Label(field.getName());
            GridPane.setHalignment(nameLabel, HPos.CENTER);
            nameLabel.setTextAlignment(TextAlignment.CENTER);
            nameLabel.setWrapText(true);
            nameLabel.setStyle("-fx-font-size: 12;");

            if (rotation == 180) nameLabel.setRotate(rotation);

            if (field.getClass() == ColorPropertyField.class) {

                // Wymiary poszczególnych fragmentów pola nieruchmości z kolorami
                gridPane.getRowConstraints().add(new RowConstraints(50 * size));          // Wysokość
                gridPane.getRowConstraints().add(new RowConstraints(100 * size));         // Wysokość
                gridPane.getRowConstraints().add(new RowConstraints(50 * size));          // Wysokość
                gridPane.getRowConstraints().add(new RowConstraints(50 * size));          // Wysokość

                StackPane stackPane = new StackPane();
                stackPane.setStyle("-fx-background-color:" + ((ColorPropertyField) field).getColor() + "; " +
                        "-fx-border-style: hidden none solid none; -fx-border-width: 2px");

                gridPane.add(stackPane, 0, 0);
                gridPane.add(nameLabel, 0, 1);

            } else {

                // Wymiary poszczególnych fragmentów innych nie kwadratowych pól
                gridPane.getRowConstraints().add(new RowConstraints(100 * size));         // Wysokość
                gridPane.getRowConstraints().add(new RowConstraints(100 * size));          // Wysokość
                gridPane.getRowConstraints().add(new RowConstraints(50 * size));          // Wysokość

                gridPane.add(nameLabel, 0, 0);

                if (field.getClass() != TaxField.class) {
                    String imgPath = "";
                    if (field.getClass() == Chance.class) { imgPath = "/img/fields/chance.png"; }
                    if (field.getClass() == CommunityChest.class) { imgPath = "/img/fields/community_chest.png"; }
                    if (field.getClass() == TransportationField.class) { imgPath = "/img/fields/transport.png"; }
                    if (field.getClass() == UtilityPropertyField.class) {
                        if (field.getPosition() == 12) { imgPath = "/img/fields/electric.png"; }
                        if (field.getPosition() == 28) { imgPath = "/img/fields/water.png"; }
                    }

                    Image image = new Image(imgPath);
                    ImageView imageView = new ImageView(image);
                    imageView.setFitHeight(100 * size);
                    imageView.setFitWidth(100 * size);

                    BorderPane pane = new BorderPane();
                    pane.setPrefSize(100 * size, 100 * size);
                    pane.setCenter(imageView);

                    if (rotation == 180 ) { pane.setRotate(180); }

                    gridPane.add(pane, 0, 1);
                }
            }

            float price = 0;
            if (field.getFieldType() == FieldType.TAX) {
                price = ((TaxField) field).getFee();
            }
            if (field.getFieldType() == FieldType.PROPERTY) {
                price = ((PropertyField) field).getCost();
            }

            if (price != 0) {
                Label priceLabel = new Label(String.valueOf(String.valueOf(price)));
                GridPane.setHalignment(priceLabel, HPos.CENTER);
                priceLabel.setStyle("-fx-font-size: 18;");
                if (field.getClass() == ColorPropertyField.class) { gridPane.add(priceLabel, 0, 3); }
                else { gridPane.add(priceLabel, 0, 2); }
                if (rotation == 180) priceLabel.setRotate(rotation);
            }

        // Pola kwadratowe (start, więzienie, parking, policjant)
        } else {
            gridPane.getColumnConstraints().add(new ColumnConstraints(250 * size));   // width
            gridPane.getRowConstraints().add(new RowConstraints(250 * size));         // height

            String imgPath = "";
            if (field.getClass() == StartField.class) { imgPath = "/img/fields/start.png"; }
            if (field.getClass() == JailField.class) { imgPath = "/img/fields/jail.png"; }
            if (field.getClass() == ParkingField.class) { imgPath = "/img/fields/parking.png"; }
            if (field.getClass() == GoToJailField.class) { imgPath = "/img/fields/gotojail.png"; }

            Image image = new Image(imgPath);
            ImageView imageView = new ImageView(image);
            imageView.setFitHeight(250 * size);
            imageView.setFitWidth(250 * size);
            gridPane.add(imageView, 0, 0);
        }

        gridPane.setRotate(rotation);

        int x = ii++; // Zmienna przechowująca indeks pola

        //Creating the mouse event handler
        EventHandler<MouseEvent> eventHandler = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                clickedFieldID_before = clickedFieldID;
                clickedFieldID = x;

                upgradePropertyBtn.setDisable(true);
                pledgePropertyBtn.setDisable(true);
                unpledgePropertyBtn.setDisable(true);

                guiFields.get(clickedFieldID).setStyle(
                        "-fx-border-color: yellow; -fx-border-width: 2px");
                guiFields.get(clickedFieldID_before).setStyle(
                        "-fx-border-color: black; -fx-border-width: 1px");

//                if (!endOfTurn) {
                    Field field = game.getBoard().getFields().get(clickedFieldID);
                    if (field.getFieldType().equals(FieldType.PROPERTY)) {
                        if (((PropertyField) field).getOwner() != null &&
                                ((PropertyField) field).getOwner().equals(game.getActivePlayer())) {

                            if (((PropertyField) field).getPropertyType().equals(PropertyType.COLOR_PROPERTY)) {

                                ColorPropertyField property = (ColorPropertyField) field;

                                if (property.getPropertyLevel() < 4 &&
                                        property.getHousesCost() <= game.getActivePlayer().getCash()) {

                                    upgradePropertyBtn.setText("Wybuduj domek");
                                    upgradePropertyBtn.setDisable(false);

                                } else if (property.getPropertyLevel() < 5 &&
                                        property.getHotelCost() <= game.getActivePlayer().getCash()) {

                                    upgradePropertyBtn.setText("Wybuduj hotel");
                                    upgradePropertyBtn.setDisable(false);
                                }
                            }
                            pledgePropertyBtn.setDisable(((PropertyField) field).isPledged());
                            unpledgePropertyBtn.setDisable(!((PropertyField) field).isPledged());
                        }
                    }
                }
//            }
        };

        //Registering the event filter
        gridPane.addEventFilter(MouseEvent.MOUSE_CLICKED, eventHandler);

        gridPane.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                Field field = game.getBoard().getFieldByID(x);
                if (field.getFieldType().equals(FieldType.PROPERTY)) {
                    GridPane pane = createPropertyCard(field);
                    pane.setTranslateX(250 * theSize + 4 * 150 * theSize);
                    pane.setTranslateY(250 * theSize + 4 * 150 * theSize);
                    guiClickedFieldCard = pane;
                    guiBoard.getChildren().add(pane);
                }
            }
        });

        gridPane.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                guiBoard.getChildren().remove(guiClickedFieldCard);
            }
        });

        return gridPane;
    }

    private GridPane createPropertyCard(Field field) {
        if (field.getFieldType().equals(FieldType.PROPERTY)) {
            PropertyField field1 = (PropertyField) field;
            GridPane gridPane = new GridPane();

            gridPane.getRowConstraints().add(new RowConstraints(100 * theSize));
            gridPane.getRowConstraints().add(new RowConstraints(400 * theSize));
            gridPane.getColumnConstraints().add(new ColumnConstraints(300 * theSize));

            Label nameLabel = new Label();
            StackPane stackPane = new StackPane();
            GridPane.setFillWidth(stackPane, true);
            GridPane.setFillHeight(stackPane, true);

            gridPane.add(stackPane, 0, 0);
            stackPane.getChildren().add(nameLabel);
            nameLabel.setText(field.getName());

            nameLabel.setStyle("-fx-font-size: 12px; -fx-font-weight: bold");

            String info = "";
            TextArea textArea = new TextArea();

            if (field1.getPropertyType().equals(PropertyType.COLOR_PROPERTY)) {
                ColorPropertyField colorPropertyField = (ColorPropertyField) field;

                stackPane.setStyle("-fx-background-color:" + colorPropertyField.getColor());

                info = "Czynsz: " + field1.getRentCost()[0] +
                        "\nZ 1 domkiem: " + field1.getRentCost()[1] +
                        "\nZ 2 domkami: " + field1.getRentCost()[2] +
                        "\nZ 3 domkami: " + field1.getRentCost()[3] +
                        "\nZ 4 domkami: " + field1.getRentCost()[4] +
                        "\nZ   hotelem: : " + field1.getRentCost()[5] +
                        "\n\nWartość zastawu: " + field1.getMortgageValue() +
                        "\n\nCena domku: " + colorPropertyField.getHousesCost() +
                        "\nCena hotelu: " + colorPropertyField.getHousesCost();

            } else {
                if (field1.getPropertyType().equals(PropertyType.TRANSPORTATION)) {
                    TransportationField transportationField = (TransportationField)field;
                    info = "Czynsz: " + transportationField.getRentCost()[0] +
                            "\nJeśli posiada 2: " + transportationField.getRentCost()[1] +
                            "\nJeśli posiada 3: " + transportationField.getRentCost()[2] +
                            "\nJeśli posiada 4: " + transportationField.getRentCost()[3] +
                            "\n\nWartość zastawu: " + field1.getMortgageValue();

                } else if (field1.getPropertyType().equals(PropertyType.UTILITY)) {
                    UtilityPropertyField utilityPropertyField = (UtilityPropertyField) field;
                    info = "Czynsz: " + utilityPropertyField.getRentCost()[0] + "\nZa każde\nwyrzucone oczko" +
                            "\n\nWartość zastawu: " + field1.getMortgageValue();
                }
            }

            textArea.setText(info);
            gridPane.add(textArea, 0, 1);

            return gridPane;

        } else {
            throw new RuntimeException();
        }
    }

    @FXML
    void upgradePropertyBtnAction(ActionEvent actionEvent) {
        Field field = game.getBoard().getFields().get(clickedFieldID);
        if (field.getFieldType().equals(FieldType.PROPERTY)) {
            if (((PropertyField)field).getPropertyType().equals(PropertyType.COLOR_PROPERTY)) {

                ColorPropertyField field1 = (ColorPropertyField) field;

                // Nic nie rób jeśli poziom posiadłości jest równy 6 (hotel jest już wybudowany)
                if (field1.getPropertyLevel() >= 6) {
                    return;
                }

                Bounds bounds = guiFields.get(clickedFieldID).
                        localToScene(guiFields.get(clickedFieldID).getBoundsInLocal());

                if (!guiHouses.containsKey(field1.getPosition())) {
                    guiHouses.put(field1.getPosition(), new ArrayList<>());
                }

                double x = bounds.getMinX();
                double y = bounds.getMinY();

                field1.upgradeProperty();
                if (field1.getPropertyLevel() < 5) {
                    game.getActivePlayer().takeCash(field1.getHousesCost());
                } else {
                    game.getActivePlayer().takeCash(field1.getHotelCost());
                }

                Image image;
                List<ImageView> imageViews = guiHouses.get(field1.getPosition());

                if (field1.getPropertyLevel() < 5) {
                    image = new Image("/img/house.png");

                    // Gdy posiadłosć ma już 4 domki i chcemy wyboduwać hotel
                } else {
                    image = new Image("/img/hotel.png");
                    for (ImageView imageView : imageViews) {
                        guiBoard.getChildren().remove(imageView);
                    }
                    imageViews.clear();
                }

                ImageView imageView = new ImageView(image);
                imageViews.add(imageView);

                imageView.setFitHeight(50 * theSize);
                imageView.setFitWidth(50 * theSize);

                // Pola pionowe lewe
                if (field1.getPosition() > 10 && field1.getPosition() < 20) {
                    imageView.setTranslateY(y + 20 * theSize * imageViews.indexOf(imageView));
                    imageView.setTranslateX(x + 250 * theSize - 50 * theSize);

                // Pola pionowe prawe
                } else if (field1.getPosition() > 30 && field1.getPosition() < 40) {
                    imageView.setTranslateY(y + 20 * theSize * imageViews.indexOf(imageView));
                    imageView.setTranslateX(x);

                // Pola o góry
                } else if (field1.getPosition() > 20 && field1.getPosition() < 30) {
                    imageView.setTranslateX(x + 20 * theSize * imageViews.indexOf(imageView));
                    imageView.setTranslateY(y + 250 * theSize - 50 * theSize);

                // Pola u dołu
                } else {
                    imageView.setTranslateX(x + 20 * theSize * imageViews.indexOf(imageView));
                    imageView.setTranslateY(y);
                }

                guiBoard.getChildren().add(imageView);
                if (field1.getPropertyLevel() == 4) {
                    upgradePropertyBtn.setText("Wybuduj hotel");
                }
                if (field1.getPropertyLevel() == 5) {
                    upgradePropertyBtn.setDisable(true);
                }
            }
        }
        printPlayerInfo();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();
    }

    @FXML
    void saveGameBtnAction(ActionEvent actionEvent) {

    }

    @FXML
    void unpledgePropertyBtnAction(ActionEvent actionEvent) {
        Field field = game.getBoard().getFields().get(clickedFieldID);

        if (field.getFieldType().equals(FieldType.PROPERTY)) {
            PropertyField propertyField = (PropertyField) field;
            propertyField.setPledged(false);
            game.getActivePlayer().takeCash(propertyField.getMortgageValue() * 1.1f);

            guiFields.get(clickedFieldID).setBackground(null);
            pledgePropertyBtn.setDisable(false);
            unpledgePropertyBtn.setDisable(true);
            printPlayerInfo();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();
        }
    }

    @FXML
    void pledgePropertyBtnAction(ActionEvent actionEvent) {
        Field field = game.getBoard().getFields().get(clickedFieldID);

        if (field.getFieldType().equals(FieldType.PROPERTY)) {
            PropertyField propertyField = (PropertyField) field;
            propertyField.setPledged(true);
            game.getActivePlayer().addCash(propertyField.getMortgageValue());

            guiFields.get(clickedFieldID).setBackground(
                    new Background(new BackgroundFill(Color.GREY, CornerRadii.EMPTY, Insets.EMPTY)));
            pledgePropertyBtn.setDisable(true);
            unpledgePropertyBtn.setDisable(false);
            printPlayerInfo();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();
        }
    }

    private void clearOptions() {
        leftDiceIV.setImage(null);
        rightDiceIV.setImage(null);
        pledgePropertyBtn.setDisable(true);
        unpledgePropertyBtn.setDisable(true);
        upgradePropertyBtn.setDisable(true);
        useJailFreeCardBtn.setDisable(true);
        payToLeaveJailBtn.setDisable(true);
        printPlayerInfo();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();
    }

    private void endOfTurnOptions() {
        pledgePropertyBtn.setDisable(true);
        unpledgePropertyBtn.setDisable(true);
        upgradePropertyBtn.setDisable(true);
        useJailFreeCardBtn.setDisable(true);
        payToLeaveJailBtn.setDisable(true);
        printPlayerInfo();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();
    }

    @FXML
    void useJailFreeCardBtnAction(ActionEvent actionEvent) {
        useJailFreeCardBtn.setDisable(true);
        game.useJailCard();
        printPlayerInfo();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();
    }

    public Board initBoard() {
        List<Field> fields = new ArrayList<>();

        fields.add(new StartField("Start", 0, 100, FieldType.START));

        fields.add(new ColorPropertyField("Ulica Konopacka", "Brown", 1, 60, 5, 25,
                new float[]{10,20,40,80,160,320}, 30, FieldType.PROPERTY, PropertyType.COLOR_PROPERTY));

        fields.add(new Chance("Szansa", 2, FieldType.CHANCE));

        fields.add(new ColorPropertyField("Ulica Stalowa", "Brown", 3, 60, 5, 25,
                new float[]{10,20,40,80,160,320}, 30, FieldType.PROPERTY, PropertyType.COLOR_PROPERTY));

        fields.add(new TaxField("Podatek", 4, 15, FieldType.TAX));

        fields.add(new TransportationField("Dworzec Zachodni", 5, FieldType.PROPERTY,
                50, new float[]{10, 20, 30, 40}, 25, PropertyType.TRANSPORTATION));

        fields.add(new ColorPropertyField("Ulica Radzyminska", "Cyan", 6, 100, 10, 50,
                new float[]{20,40,80,160,320,640}, 50, FieldType.PROPERTY, PropertyType.COLOR_PROPERTY));

        fields.add(new Chance("Szansa", 7, FieldType.CHANCE));

        fields.add(new ColorPropertyField("Ulica Jagiellonska", "Cyan", 8, 100, 10, 50,
                new float[]{20,40,80,160,320,640}, 50, FieldType.PROPERTY, PropertyType.COLOR_PROPERTY));

        fields.add(new ColorPropertyField("Ulica Targowa", "Cyan", 9, 120, 15, 75,
                new float[]{20,40,80,160,320,640}, 50, FieldType.PROPERTY, PropertyType.COLOR_PROPERTY));

        fields.add(new JailField("Więzienie", 10, FieldType.JAIL));

        fields.add(new ColorPropertyField("Ulica Plowiecka", "Magenta", 11, 140, 20, 100,
                new float[]{25,50,100,200,400,800}, 70, FieldType.PROPERTY, PropertyType.COLOR_PROPERTY));

        fields.add(new UtilityPropertyField("Elektrownia", 12, FieldType.PROPERTY, 150,
                new float[]{1},75, PropertyType.UTILITY));

        fields.add(new ColorPropertyField("Ulica Marsa", "Magenta", 13, 140, 20, 100,
                new float[]{25,50,100,200,400,800}, 70, FieldType.PROPERTY, PropertyType.COLOR_PROPERTY));

        fields.add(new ColorPropertyField("Ulica Grochowska", "Magenta", 14, 160, 25, 125,
                new float[]{30,60,120,240,480,960}, 80, FieldType.PROPERTY, PropertyType.COLOR_PROPERTY));

        fields.add(new TransportationField("Dworzec Gdanski", 15, FieldType.PROPERTY,
                50, new float[]{10, 20, 30, 40}, 25, PropertyType.TRANSPORTATION));

        fields.add(new ColorPropertyField("Ulica Obozowa", "Orange", 16, 180, 30, 150,
                new float[]{30,60,120,240,480,960}, 90, FieldType.PROPERTY, PropertyType.COLOR_PROPERTY));

        fields.add(new CommunityChest("Kasa społeczna", 17, FieldType.CHANCE));

        fields.add(new ColorPropertyField("Ulica Gorczewska", "Orange", 18, 180, 30, 150,
                new float[]{30,60,120,240,480,960}, 90, FieldType.PROPERTY, PropertyType.COLOR_PROPERTY));

        fields.add(new ColorPropertyField("Ulica Obozowa", "Orange", 19, 200, 35, 175,
                new float[]{35,70,140,280,560,1120}, 100, FieldType.PROPERTY, PropertyType.COLOR_PROPERTY));

        fields.add(new ParkingField("Parking", 20, FieldType.PARKING));

        fields.add(new ColorPropertyField("Ulica Mickiewicza", "Red", 21, 220, 35, 175,
                new float[]{35,70,140,280,560,1120}, 110, FieldType.PROPERTY, PropertyType.COLOR_PROPERTY));

        fields.add(new Chance("Szansa", 22, FieldType.CHANCE));

        fields.add(new ColorPropertyField("Ulica Slowackiego", "Red", 23, 220, 40, 200,
                new float[]{40,80,160,320,640,1280}, 110, FieldType.PROPERTY, PropertyType.COLOR_PROPERTY));

        fields.add(new ColorPropertyField("Plac Wilsona", "Red", 24, 240, 40, 200,
                new float[]{40,80,160,320,640,1280}, 120, FieldType.PROPERTY, PropertyType.COLOR_PROPERTY));

        fields.add(new TransportationField("Dworzec Wschodni", 25, FieldType.PROPERTY,
                50, new float[]{10, 20, 30, 40}, 25, PropertyType.TRANSPORTATION));

        fields.add(new ColorPropertyField("Ulica Swietokrzyska", "Yellow", 26, 260, 45, 225,
                new float[]{40,80,160,320,640,1280}, 130, FieldType.PROPERTY, PropertyType.COLOR_PROPERTY));

        fields.add(new ColorPropertyField("Krakowskie Przedmiescia", "Yellow", 27, 260, 45, 225,
                new float[]{40,80,160,320,640,1280}, 130, FieldType.PROPERTY, PropertyType.COLOR_PROPERTY));

        fields.add(new UtilityPropertyField("Wodociagi", 28, FieldType.PROPERTY, 150,
                new float[]{1},75, PropertyType.UTILITY));

        fields.add(new ColorPropertyField("Nowy Swiat", "Yellow", 29, 280, 45, 225,
                new float[]{45,90,180,360,720,1440}, 140, FieldType.PROPERTY, PropertyType.COLOR_PROPERTY));

        fields.add(new GoToJailField("Policja", 30, FieldType.GOTOJAIL));

        fields.add(new ColorPropertyField("Plac Trzech Krzyzy", "Green", 31, 300, 50, 250,
                new float[]{50,100,200,400,800,1600}, 150, FieldType.PROPERTY, PropertyType.COLOR_PROPERTY));

        fields.add(new ColorPropertyField("Ulica Marszalkowska", "Green", 32, 300, 50, 250,
                new float[]{50,100,200,400,800,1600}, 150, FieldType.PROPERTY, PropertyType.COLOR_PROPERTY));

        fields.add(new CommunityChest("Kasa społeczna", 33, FieldType.CHANCE));

        fields.add(new ColorPropertyField("Aleje Jerozolimskie", "Green", 34, 320, 50, 250,
                new float[]{50,100,200,400,800,1600}, 160, FieldType.PROPERTY, PropertyType.COLOR_PROPERTY));

        fields.add(new TransportationField("Dworzec Centralny", 35, FieldType.PROPERTY,
                50, new float[]{10, 20, 30, 40}, 25, PropertyType.TRANSPORTATION));

        fields.add(new Chance("Szansa", 36, FieldType.CHANCE));

        fields.add(new ColorPropertyField("Ulica Belwederska", "Blue", 37, 350, 55, 275,
                new float[]{55,110,220,440,880,1760}, 175, FieldType.PROPERTY, PropertyType.COLOR_PROPERTY));

        fields.add(new TaxField("Domiar Podatkowy", 38, 20, FieldType.TAX));

        fields.add(new ColorPropertyField("Aleje Ujazdowskie", "Blue", 39, 400, 70, 350,
                new float[]{70,140,280,560,1120,2240}, 200, FieldType.PROPERTY, PropertyType.COLOR_PROPERTY));

        return new Board(40, fields, 200);
    }

    public void communityChanceWindow(String fieldType, String content) {
        String title = "";
        if (fieldType.equals("co")) { title = "Kasa społeczna"; }
        else if (fieldType.equals("ch")) { title = "Szansa"; }
        else if (fieldType.equals("wi")) { title = "Policjant"; }

        Dialog<String> dialog = new Dialog<String>();
        dialog.setTitle(title);
        dialog.setContentText(content);
        ButtonType type = new ButtonType("Ok", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().add(type);
        dialog.showAndWait();
        printPlayerInfo();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();
    }

    public void payToLeaveJailBtnAction(ActionEvent actionEvent) {
        if (game.getActivePlayer().getCash() >= 50) {
            game.getActivePlayer().setInJail(false);
        }
        printPlayerInfo();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();
    }

    private Node getNodeByRowColumnIndex (final int row, final int column, GridPane gridPane) {
        Node result = null;
        ObservableList<Node> childrens = gridPane.getChildren();

        for (Node node : childrens) {
            if(gridPane.getRowIndex(node) == row && gridPane.getColumnIndex(node) == column) {
                result = node;
                break;
            }
        }

        return result;
    }
    private void checkIfBancrupt() {
        if (game.getActivePlayer().getBankrupt()) {
            Dialog<String> dialog = new Dialog<String>();
            dialog.setTitle("bankructwo!");
            dialog.setContentText("Gracz " + game.getActivePlayer().getName() + " jest bankrutem!");
            ButtonType type = new ButtonType("Ok", ButtonBar.ButtonData.OK_DONE);
            dialog.getDialogPane().getButtonTypes().add(type);
            dialog.showAndWait();
            System.exit(0);
            printPlayerInfo();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();checkIfBancrupt();
        }
    }
}