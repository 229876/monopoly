package pl.monopoly.view;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

public class JavaFX extends Application {

    private static Stage stage;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        JavaFX.stage = primaryStage;
        primaryStage.setResizable(false);
        setScene("/fxml/menu.fxml", "Monopoly");
    }

    private static Parent loadFxml(String fxml) {
        try {
            return new FXMLLoader(JavaFX.class.getResource(fxml)).load();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void setScene(String filePath, String title) {
        stage.setScene(new Scene(Objects.requireNonNull(loadFxml(filePath))));
        stage.setTitle(title);
        stage.sizeToScene();
        stage.show();
    }
}