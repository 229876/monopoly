package pl.monopoly;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pl.monopoly.model.fields.FieldType;
import pl.monopoly.model.fields.StartField;
import pl.monopoly.model.fields.TaxField;
import pl.monopoly.model.Player;

class GameTest {

    @Test
    public void test1() {
        StartField startField = new StartField("Start", 0, 300, FieldType.START);

        TaxField taxField1 = new TaxField("PodatekDochodowy", 4, 100, FieldType.TAX);

        TaxField taxField2 = new TaxField("PodatekOdNieruch", 32, 200, FieldType.TAX);

        Player player1 = new Player("Kunz", "red", 150);

        Assertions.assertEquals(450, player1.getCash());
        player1.takeCash(300);
        taxField1.PayFee(player1);
        Assertions.assertEquals(50, player1.getCash());
        taxField2.PayFee(player1);
        Assertions.assertTrue(player1.getBankrupt());
    }
}