package pl.monopoly.model.fields;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pl.monopoly.model.Board;
import pl.monopoly.model.Game;
import pl.monopoly.model.Player;

import java.util.ArrayList;
import java.util.List;

class ColorPropertyFieldTest {

    @Test
    void test1() {
        Player player1 = new Player("Kunz", "red", 100);
        Player player2 = new Player("Iras", "blue", 300);
        float[] rentCost = {20, 30, 40, 50, 100};
        ColorPropertyField jez = new ColorPropertyField("Jezowska", "green", 13, 100, 10, 50, rentCost, 75, FieldType.PROPERTY, PropertyType.COLOR_PROPERTY);
        jez.setOwner(player1);
        jez.upgradeProperty();
        jez.upgradeProperty();
        jez.payRent(player2);
        Assertions.assertEquals(260, player2.getCash());
    }

    @Test
    void test2() {
        float[] purpleProperties = {20, 30, 40, 50, 60};
        ColorPropertyField colorPropertyField1 = new ColorPropertyField("jezowska", "blue", 0, 50, 10, 40, new float[]{10, 20, 30, 40, 50}, 20, FieldType.PROPERTY, PropertyType.COLOR_PROPERTY);
        ColorPropertyField colorPropertyField2 = new ColorPropertyField("polna", "red", 1, 60, 15, 60, purpleProperties, 25, FieldType.PROPERTY, PropertyType.COLOR_PROPERTY);
        List<Field> fields = new ArrayList<>();
        fields.add(colorPropertyField1);
        fields.add(colorPropertyField2);

        Board board = new Board(2, fields, 200);

        List<Player> players = new ArrayList<>();
        String[] playerNames = new String[]{"alfa", "beta", "gamma", "delta"};
        String[] colors = new String[]{"blue", "red", "yellow", "green"};
        for (int i = 0; i < 4; i++) {
            players.add(new Player(playerNames[i], colors[i], board.getStartingCash()));
        }

        Game game = new Game(players, board);
    }
}