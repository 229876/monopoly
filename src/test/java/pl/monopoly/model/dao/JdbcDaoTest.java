package pl.monopoly.model.dao;

import java.util.List;
import java.util.ArrayList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import pl.monopoly.model.*;
import pl.monopoly.model.fields.Field;

public class JdbcDaoTest {

    @Test
    public void savePlayerAndLoadPlayerTest() throws Exception {

        Player player1 = new Player("name1","color1",1200.0f);
        Player player2 = new Player("name2","color2",1200.0f);
        List<Player> players = new ArrayList<>();
        players.add(player1);
        players.add(player2);
        try (JdbcPlayerDao dao = new JdbcPlayerDao()){
            dao.write(player1);
            player2 = dao.read();
            assertNotEquals(player1,player2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getBoardTest() {
        Board board;

        try (JdbcBoardDao dao = new JdbcBoardDao()) {
            dao.setName("");
            board = dao.read();

            for (Field field : board.getFields()) {
                System.out.println(field.toString());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}